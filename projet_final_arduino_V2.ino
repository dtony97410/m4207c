#include <LWiFi.h>
#include <LWiFiClient.h>
#include <LGPS.h>
#include <rgb_lcd.h>
#include <LBattery.h>
#include <Wire.h>
#include "MMA7660.h"
#define WIFI_AP "RT-WIFI-TMP"
#define WIFI_PASSWORD "IUTRT97410"
#define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
#define SITE_URL "192.168.50.74"
MMA7660 accelemeter;
rgb_lcd ecranRGB;
LWiFiClient c;

///////////////////////////////////////// DEFINITION DES PORTS ///////////////////////////////////////////////////////////
const int potentiometer = A0; 
const int pinButton = 3;                        // pin of button define here
const int pinLed = 7;                        // pin of led define here
const int buzzer = 6;
int x; // Création des variables pour les 3 accélérations 
int y;
int z;
boolean disconnectedMsg = false;

////////////////////////////////// FIN FONCTION //////////////////////////////////////////////////////////////////////


gpsSentenceInfoStruct info;
char buff[256];

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{ 
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  double latitude;
  double longitude;
  int tmp,hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

////////////////////////////////// DEBUT FONCTION //////////////////////////////////////////////////////////////////////
  

void axelerometre(){
    
    int8_t x;
    int8_t y;
    int8_t z;
    
    
    float ax, ay, az;

    accelemeter.getAcceleration(&ax, &ay, &az);
    Serial.println("accleration of X/Y/Z: ");
    Serial.print(ax);
    Serial.println(" g");
    Serial.print(ay);
    Serial.println(" g");
    Serial.print(az);
    Serial.println(" g");
    Serial.println("*************");
    delay(500);

    if (ax<-80 || ay<-0.05 || az<-0.00) {
    digitalWrite(pinLed, HIGH);// led on
    digitalWrite(buzzer, HIGH);// Faire du bruit
    }
    
    else {
    digitalWrite(pinLed, LOW);// led on
    }

}

void wifi (){
  
  // Make sure we are connected, and dump the response content to Serial
  while (c)
  {
    int v = c.read();
    if (v != -1)
    {
      Serial.print((char)v);
    }
    else
    {
      Serial.println("no more content, disconnect");
      c.stop();
      while (1)
      {
        delay(1);
      }
    }
  }

  if (!disconnectedMsg)
  {
    Serial.println("disconnected by server");
    disconnectedMsg = true;
  }
  delay(500);
}
////////////////////////////////// FIN FONCTION //////////////////////////////////////////////////////////////////////

void setup() {
    boolean disconnectedMsg = false;
  // put your setup code here, to run once:
    accelemeter.init();  
    Serial.begin(115200);
    pinMode(pinButton, INPUT);                  // set button INPUT
    pinMode(pinLed, OUTPUT);                    // set led OUTPUT
    pinMode(buzzer,OUTPUT);// Définir la sortie du buzzer
    LGPS.powerOn();
    Serial.println("LGPS Power on, and waiting ..."); 
    delay(3000);
    ecranRGB.begin(16,2, 0x00);
    digitalWrite(pinButton, LOW);// led on
    int b = digitalRead(pinButton);
    Serial.println("Button dans setup");
    Serial.println(b);
    delay(1000);
    pinMode(potentiometer, INPUT);                  // set button INPUT
    //////// setup wifi //////
     LWiFi.begin();

    // keep retrying until connected to AP
    Serial.println("Connecting to AP");
    while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
    {
     delay(1000);
    }

  // keep retrying until connected to website
  Serial.println("Connecting to WebSite");
  while (0 == c.connect(SITE_URL, 80))
  {
    Serial.println("Re-Connecting to WebSite");
    delay(1000);
  }
  // send HTTP request, ends with 2 CR/LF
  Serial.println("send HTTP GET request");
  c.println("GET / HTTP/1.1");
  c.println("Host: " SITE_URL "/file.php?b=1");
  c.println("Connection: close");
  c.println();

  // waiting for server response
  Serial.println("waiting HTTP response:");
  while (!c.available())
  {
    delay(100);
  }

}

void loop() {
  Serial.println("Debut loop");
  char message[256];
  int b = digitalRead(pinButton);
  int a = analogRead(potentiometer);
  Serial.println(a);
  
  if (a == 1023) {
  digitalWrite(pinLed, HIGH);// led on
  delay(5000);
  digitalWrite(pinLed, LOW);//led off
  ecranRGB.clear();
  ecranRGB.setCursor(0,0);
  ecranRGB.print("Alarme Eteinte");
  }
  
  else {
  
  // put your main code here, to run repeatedly:
  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA);
  axelerometre(); 
  ecranRGB.setCursor(0,0);
  sprintf(message,"Batterylevel=%d", LBattery.level() );
  ecranRGB.print(message);
  delay(5000);
  wifi();
  
  }
}